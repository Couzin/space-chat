from fastapi import FastAPI
from fastapi.exceptions import RequestValidationError, HTTPException
from fastapi.middleware.cors import CORSMiddleware
from fastapi.responses import JSONResponse
from routes.ask import router as ask_router
from utils.error_handler import handle_error, LangException

app = FastAPI()

app.add_middleware(
    CORSMiddleware,
    allow_origins=["*"],
    allow_credentials=True,
    allow_methods=["*"],
    allow_headers=["*"]
)


@app.exception_handler(RequestValidationError)
async def error_handler(request, exc):
    return JSONResponse(
        status_code=422,
        content={"message": exc.errors()[0]["msg"]}
    )


@app.exception_handler(Exception)
async def error_handler(request, exc):
    return JSONResponse(
        status_code=500,
        content={"message": exc}
    )


@app.exception_handler(HTTPException)
async def error_handler(request, exc):
    return JSONResponse(
        status_code=exc.status_code,
        content={"message": exc.detail}
    )

# Including routers here
app.include_router(ask_router, prefix="/api/ask", tags=["ask"])


@app.get("/")
async def root():
    return {"message": "Hello World"}


@app.get("/api")
async def api_root():
    return {"message": "api root"}

