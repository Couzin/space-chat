from typing import List

from pydantic import BaseModel

from models.Ask import Ask
from models.BotResponse import BotResponse


class Exchange(BaseModel):
    question: Ask
    response: BotResponse

    def to_dict(self):
        return {
            "question": self.question.model_dump(exclude={"token"}),
            "response": self.response.model_dump(exclude={"token"})
        }


class History(BaseModel):
    token: str
    history: List[Exchange]
