from fastapi import HTTPException
from fastapi.exceptions import RequestValidationError


class LangException(Exception):
    def __init__(self, lang):
        self.lang = lang

    def __str__(self):
        return f"Language {self.lang} is not supported"


def handle_error(error):
    if isinstance(error, HTTPException):
        raise HTTPException(status_code=error.status_code, detail=error.detail)
    if isinstance(error, RequestValidationError):
        raise HTTPException(status_code=422, detail=error.errors()[0]["msg"])
    if isinstance(error, LangException):
        raise HTTPException(status_code=422, detail=str(error))
    if isinstance(error, Exception):
        raise error
